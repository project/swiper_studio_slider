<?php

namespace Drupal\swiper_studio_slider\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBase;
use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Defines the swiper configuration entity .
 *
 * @ConfigEntityType(
 *   id = "swiper_studio_slider",
 *   label = @Translation("Swiper optionset"),
 *   handlers = {
 *   "list_builder" =
 *   "Drupal\swiper_studio_slider\Controller\SwiperListBuilder",
 *     "form" = {
 *       "add" = "Drupal\swiper_studio_slider\Form\SwiperSliderForm",
 *       "edit" = "Drupal\swiper_studio_slider\Form\SwiperSliderForm",
 *       "delete" = "Drupal\swiper_studio_slider\Form\SwiperSliderDeleteForm"
 *     }
 *   },
 *   config_prefix = "options",
 *   admin_permission = "administer swiper_slider",
 *   entity_keys = {
 *
 *  "id" = "name",
 *     "label" = "label",
 *     "status" = "status",
 *     "weight" = "weight",
 *   },
 *   links = {
 *    "canonical" = "/admin/config/media/swiper-slider/{swiper}",
 *     "edit-form" = "/admin/config/media/swiper-slider/{swiper}/edit",
 *     "enable" = "/admin/config/media/swiper-slider/{swiper}/enable",
 *     "disable" = "/admin/config/media/swiper-slider/{swiper}/disable",
 *     "delete-form" = "/admin/config/media/swiper-slider/{swiper}/delete",
 *     "collection" = "/admin/config/media/swiper-slider"
 *   },
 *   config_export = {
 *     "id",
 *     "name",
 *     "weight",
 *     "label",
 *     "group",
 *     "skin",
 *     "breakpoints",
 *     "optimized",
 *     "options",
 *   }
 * )
 */
class SwiperSlider extends ConfigEntityBase implements ConfigEntityInterface {

}
